import { Browser } from 'selenium-webdriver';

type GepettoProperties = {
  initialPath: string;
  browser: string;
  htmlPath: string;
};

const properties: GepettoProperties = {
  initialPath: 'http://localhost:9100/',
  browser: Browser.CHROME,
  htmlPath: 'gepetto-html',
};

export default properties;
