import { Builder, Browser, By, until, WebDriver } from 'selenium-webdriver';
import chrome from 'selenium-webdriver/chrome';
import * as cheerio from 'cheerio';
import properties from './properties';
import fsp from 'fs-promise';
import { v4 as uuidv4 } from 'uuid';

export class GepettoWebDriver {
  driver!: WebDriver;
  readonly options;

  constructor() {
    this.options = new chrome.Options();
    // User agent
    // this.options.addArguments(
    //   'user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36'
    // );
    // Profile
    const profilePath = '/src-ssr/auto/gepetto-profile';
    this.options.addArguments(`user-data-dir=${profilePath}`);
    // Desativa o flag do WebDriver
    this.options.addArguments('--disable-blink-features=AutomationControlled');
    // Configura outras opções que possam ajudar a parecer menos automatizado
    this.options.addArguments('--start-maximized');
    this.options.addArguments('--disable-infobars');
    this.options.addArguments('--no-sandbox');
    this.options.addArguments('--disable-dev-shm-usage');
    this.options.excludeSwitches('enable-automation');

    this._init();
  }

  async _init() {
    console.log('>>> Inicializando...');
    this.driver = await new Builder()
      .forBrowser(properties.browser)
      .setChromeOptions(this.options)
      .build();
    console.log('>>> Inicializou...');
    await this.driver.get(properties.initialPath);
  }

  async _initWithProxy(host: string, port: string) {
    // Substitua estes valores pelo seu host e porta de proxy reais
    const proxyOptions = `${host}:${port}`;

    // Configurando o proxy
    this.options.addArguments(`--proxy-server=http://${proxyOptions}`);

    // Se necessário, você pode adicionar mais opções do Chrome aqui
    // por exemplo, rodar em modo headless: chromeOptions.addArguments('--headless');

    // Iniciar o Chrome com as opções configuradas
    this.driver = await new Builder()
      .forBrowser('chrome')
      .setChromeOptions(this.options)
      .build();
  }

  async wait(segundos: number): Promise<void> {
    await this.driver.wait(
      new Promise((resolve, _) => {
        setTimeout(() => resolve(0), segundos * 1000);
      })
    );
  }

  async newTab(url: string) {
    await this.driver.switchTo().newWindow('tab');
    // Obtendo handles de todas as abas/janelas abertas
    const windows = await this.driver.getAllWindowHandles();
    // Alternando para a nova aba, assumindo que ela seja a segunda aba
    await this.driver.switchTo().window(windows[windows.length - 1]);
    // Você pode agora navegar na nova aba se desejar
    await this.driver.get(url);
  }

  async mainTab() {
    const windows = await this.driver.getAllWindowHandles();
    // Feche todas as abas exceto a primeira
    for (let i = 1; i < windows.length; i++) {
      await this.driver.switchTo().window(windows[i]);
      await this.driver.close();
    }
    await this.driver.switchTo().window(windows[0]);
  }

  async waitForCloudFlareByPass() {
    await this.driver.wait(
      until.titleIs('Mangás Chan – Leia seus mangás online e gratuitamente')
    );
  }

  async *load(url: string) {
    try {
      await this.newTab(url);
      // await this.waitForCloudFlareByPass(driver);
    } finally {
      yield this;
    }
  }

  async scraper<T>(fn: ($: cheerio.CheerioAPI) => T): Promise<T> {
    const driver = this.driver;
    const html = await driver.getPageSource();
    const $ = cheerio.load(html);
    return fn($);
  }

  // ----------------------------- DEBUG

  async saveHtml($: cheerio.CheerioAPI) {
    const $fsp = fsp as any;
    try {
      // Verificar se a pasta existe; se não, criar
      try {
        await $fsp.access(properties.htmlPath);
      } catch (error) {
        // Se a pasta não existir, um erro será lançado, então criamos a pasta
        await $fsp.mkdir(properties.htmlPath, { recursive: true });
      }

      // Salvar o arquivo HTML
      const filePath = `${properties.htmlPath}/${uuidv4()}.html`;
      await $fsp.writeFile(filePath, $.html(), 'utf8');
      console.log('Arquivo salvo com sucesso!');
    } catch (erro) {
      // Tratar qualquer erro que ocorra durante a verificação/criação da pasta ou salvamento do arquivo
      console.error('Erro ao salvar o arquivo:', erro);
      throw erro; // Relançar o erro para ser tratado por quem chama a função, se necessário
    }
  }
}
