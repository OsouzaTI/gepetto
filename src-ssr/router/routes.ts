import { Router } from 'express';
import gepettoDatabase from '../auto/database';
import gepetto from '../auto/gepetto';

const express = require('express');
const router: Router = express.Router();

router.get('/gepetto', async (req, res) => {
  console.log('Requisição chegou...');
  const open = gepetto.load('https://google.com');
  for await (const gp of open) {
    const title = await gp.scraper<{ title: string; img: string }>(($) => {
      // gp.saveHtml($);
      gp.mainTab();
      return {
        title: $('title').text(),
        img: $('img').attr('src')!,
      };
    });

    res.json({ title });

    return;
  }
});

router.post('/gepetto', async (req, res) => {
  const { url, js } = req.body;
  const open = gepetto.load(url);
  // Criando a VM
  const vm = require('vm');

  for await (const gp of open) {
    gp.mainTab();
    try {
      const data = await gp.scraper<any>(($) => {
        const functionCheerio = (js: string) => `
          data = (function(){${js}})()
        `;

        const script = new vm.Script(functionCheerio(js));
        const sandbox = {
          $: $,
          data: {},
        };
        // Cria um contexto de sandbox
        const context = new vm.createContext(sandbox);
        script.runInContext(context);
        return sandbox.data;
      });
      res.json({ data });
    } catch (error) {
      res.json({ error: (error as Error).message });
    }
    return;
  }
});

router.get('/gepetto/exit', async (req, res) => {
  gepetto.driver.quit();
  res.json({ msg: 'Gepetto finalizado...' });
});

// DATABASE
router.post('/gepetto/add-task', async (req, res) => {
  const { id, title, js, url, trigger, category_id } = req.body;
  try {
    await gepettoDatabase.task.upsert({
      where: {
        id: id ?? 0, // Condição para encontrar o registro - geralmente uma chave única
      },
      update: {
        // Aqui você especifica os campos a serem atualizados se o registro for encontrado
        title: title,
        js: js,
        url: url,
        trigger: trigger,
        category_id: category_id ?? 1,
      },
      create: {
        // Aqui você especifica todos os campos necessários para criar o registro caso não exista
        title: title,
        js: js,
        url: url,
        trigger: trigger,
        category_id: category_id ?? 1,
      },
    });
    res.sendStatus(200);
  } catch (error) {
    console.log('ERROR ', error);
    res.sendStatus(500);
  }
});

router.get('/gepetto/get-tasks', async (req, res) => {
  const tasks = await gepettoDatabase.task.findMany({
    include: {
      category: true,
    },
  });
  res.json(tasks);
});

router.get('/gepetto/get-categories', async (req, res) => {
  const categories = await gepettoDatabase.category.findMany({
    include: {
      tasks: true,
    },
  });
  res.json(categories);
});

export { router };
