import { AxiosInstance } from 'axios';
import { format, formatISO, parse } from 'date-fns';
import { enUS, ptBR } from 'date-fns/locale';
import { type } from 'os';
import { defineStore } from 'pinia';

export type Category = {
  id?: number;
  description: string;
  tasks?: GepettoTask[];
};

type CategoriesNode = {
  title: string;
  children?: CategoriesNode[];
};

export type GepettoTask = {
  id?: number;
  title: string;
  url: string;
  js: string;
  trigger?: Date;
  category?: Category;
  category_id?: number;
};

type GepettoProps = {
  data: any[] | null;
  loading: boolean;
  task: GepettoTask;
  tasks: GepettoTask[];
  categories: Category[];
};

export const useGepettoStore = defineStore('gepettoStore', {
  state: (): GepettoProps => ({
    data: null,
    loading: false,
    tasks: [],
    categories: [],
    task: {
      title: '',
      url: '',
      js: '',
    },
  }),
  getters: {
    hasData: (state) => state.data != null,
    categoriesNodes: (state) => {
      return state.categories.map((category) => ({
        label: category.description,
        header: 'category',
        children: [
          ...(category.tasks?.map((task) => ({
            label: task.title,
            header: 'task',
            ...task,
          })) || []),
        ],
      }));
    },
  },
  actions: {
    setData(data: any) {
      this.data = data;
    },
    async setLoading(loading: boolean) {
      await new Promise((resolve) => setTimeout(resolve, 250));
      this.loading = loading;
    },
    async addTask(axios: AxiosInstance, task: GepettoTask) {
      await axios.post('/gepetto/add-task', {
        id: task.id,
        title: task.title,
        js: task.js,
        url: task.url,
        trigger: task.trigger ? task.trigger : '',
        category_id: task.category_id,
      });
    },
    async loadTasks(axios: AxiosInstance) {
      const _tasks = await axios.get('/gepetto/get-tasks');
      this.tasks = _tasks.data;
    },
    async loadCategories(axios: AxiosInstance) {
      const _categories = await axios.get('/gepetto/get-categories');
      this.categories = _categories.data;
    },
    async executeTask(axios: AxiosInstance, url: string, js: string) {
      this.setLoading(true);
      const scrape = await axios.post('/gepetto', {
        js: js,
        url: url,
      });
      this.setData(scrape.data);
      this.setLoading(false);
    },
    setTask(
      id: number,
      title: string,
      url: string,
      js: string,
      trigger: Date,
      category: Category
    ) {
      this.task = { id, title, url, js, trigger, category } as GepettoTask;
    },
    clearTask() {
      for (let key in this.task) {
        const _key = key as string;
        this.task[_key] = '';
      }
    },
  },
});
