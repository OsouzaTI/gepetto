import { ref, Ref } from 'vue';

// Define a estrutura para as configurações de validação de um campo.
interface FieldValidation<T> {
  validator: () => boolean; // Função de validação.
  error: string; // Mensagem de erro associada.
}

// Tipo para o objeto completo de configurações de validação.
type ValidationConfig<T> = {
  [K in keyof T]?: FieldValidation<T[K]>;
};

// Ajustando para que ErrorMapping seja um objeto chave-valor.
type ErrorMapping<T> = {
  [K in keyof T]?: string;
};

// Focus
type FocusMapping<T> = {
  [K in keyof T]?: boolean;
};

export default class Forms<T extends Record<string, any>> {
  private $values: Ref<T>;
  private $errors: Ref<ErrorMapping<T>>;
  private $focus: Ref<FocusMapping<T>>;
  private $validationConfig: ValidationConfig<T> = {};

  constructor(initialValues: T) {
    this.$values = ref<T>(initialValues);
    // Inicializando $errors como um objeto vazio em vez de um Map.
    this.$errors = ref<ErrorMapping<T>>({});

    // Inicializa $focus com false para cada chave em initialValues
    const initialFocusStates: FocusMapping<T> = Object.keys(
      initialValues
    ).reduce((acc, key) => {
      acc[key as keyof T] = false;
      return acc;
    }, {} as FocusMapping<T>);
    this.$focus = ref<FocusMapping<T>>(initialFocusStates);
  }

  // Configura validações e mensagens de erro em uma única chamada.
  setValidationConfig(config: ValidationConfig<T>) {
    this.$validationConfig = config;
  }

  validate(ignoreFocus?: boolean): boolean {
    let isValid = true;

    // Itera sobre cada configuração de validação fornecida.
    Object.entries(this.$validationConfig).forEach(([key, config]) => {
      const field = key as keyof T;

      // Ignora o campo sem foco
      let validate = true;
      if (ignoreFocus) {
        validate = config! && !config.validator();
      } else {
        const focus = this.$focus.value[field];
        validate = config! && !config.validator() && focus!;
      }

      if (validate) {
        // Se a validação falhar, define a mensagem de erro.
        // Atribuindo diretamente ao objeto de erros.
        this.$errors.value[field] = config!.error;
        isValid = false;
      } else {
        // Limpa a mensagem de erro se a validação passar.
        // Removendo a chave do objeto de erros ou setando como string vazia.
        this.$errors.value[field] = '';
      }
    });

    return isValid;
  }

  // Getters permanecem inalterados, mas agora trabalham com um objeto.
  get values() {
    return this.$values.value;
  }

  get errors() {
    return this.$errors.value;
  }

  get focus() {
    return this.$focus.value;
  }

  fieldFocus<K extends keyof T>(field: K, focus: boolean) {
    console.log(field, focus);
    this.$focus.value[field] = focus;
  }

  setValues(value: T) {
    this.$values.value = value;
  }
}
