/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,ts,vue}'],
  darkMode: 'class', // or 'media' based on your preference
  theme: {
    extend: {
      // Define custom colors or extend existing ones for dark mode
      colors: {
        dark: {
          // Example custom colors for dark theme
          background: '#222222', // Dark background
          text: '#e0e0e0', // Lighter text suitable for dark background
          header: '#0f0f0f', // A primary color for your dark theme
          secondary: '#03dac6', // A secondary color for your dark theme
          card: '#F6F5F5',
        },
      },
    },
  },
  plugins: [],
};
